export enum AuthBackend {
    BASIC = 0,
    SESSION = 1,
    KERBEROS = 2,
    TOKEN = 3,
    NONE = 4
}