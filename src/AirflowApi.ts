export type AirflowApi = {
	// DAGs
	getDags: () => Promise<any>;
	getDag: (dagId: string) => Promise<any>;
	createDagRun: (dagId: string, data: any) => Promise<any>;
	getDagRuns: (dagId: string) => Promise<any>;
	getDagRun: (dagId: string, dagRunId: string) => Promise<any>;

	// Tasks
	getTasks: (dagId: string) => Promise<any>;
	getTask: (dagId: string, taskId: string) => Promise<any>;
	getTaskInstances: (dagId: string, dagRunId: string) => Promise<any>;

	// Connections
	getConnections: () => Promise<any>;
	getConnection: (connectionId: string) => Promise<any>;
	createConnection: (data: any) => Promise<any>;
	updateConnection: (connectionId: string, data: any) => Promise<any>;
	deleteConnection: (connectionId: string) => Promise<any>;

	// Variables
	getVariables: () => Promise<any>;
	getVariable: (variableKey: string) => Promise<any>;
	createVariable: (data: any) => Promise<any>;
	updateVariable: (variableKey: string, data: any) => Promise<any>;
	deleteVariable: (variableKey: string) => Promise<any>;

	// Pools
	getPools: () => Promise<any>;
	getPool: (poolName: string) => Promise<any>;
	createPool: (data: any) => Promise<any>;
	updatePool: (poolName: string, data: any) => Promise<any>;
	deletePool: (poolName: string) => Promise<any>;

	// Logs
	getLogs: (
		dagId: string,
		dagRunId: string,
		taskId: string,
		tryNumber: number,
	) => Promise<any>;

	// Health
	getHealth: () => Promise<any>;
};
