import type { AirflowApi } from "./AirflowApi";
import type { AuthConfig } from "./AuthConfig";
import { AuthBackend } from "./AuthBackend";

import axios from "axios";
import type {
	AxiosInstance,
	AxiosRequestConfig,
	AxiosRequestHeaders,
	InternalAxiosRequestConfig,
} from "axios";

export default class AirflowClient implements AirflowApi {
	private authConfig: AuthConfig = {
		method: AuthBackend.BASIC,
		username: "admin",
		password: "",
	};

	/**
	 * Instantiates an Apache Airflow client.
	 * @param baseUrl Airflow API base URL.
	 * @param authConfig API authentication configuration.
	 */
	constructor(baseUrl: string, authConfig?: AuthConfig, axiosOptions?: any) {
		if (authConfig) this.authConfig = authConfig;

		// Setup Axios client with the given baseUrl.
		this.axiosClient = axios.create({
			baseURL: baseUrl,
			...axiosOptions
		});

		// Setup Axios middleware to handle different authentication methods.
		this.axiosClient.interceptors.request.use(
			(config: InternalAxiosRequestConfig) => {
				// Ensure headers object is defined.
				if (!config.headers) config.headers = {} as AxiosRequestHeaders;

				switch (this.authConfig.method) {
					case AuthBackend.BASIC: {
						// Basic Authentication
						const basicAuth = btoa(
							`${this.authConfig.username}:${this.authConfig.password}`,
						);
						config.headers.Authorization = `Basic ${basicAuth}`;
						break;
					}

					// case AuthBackend.SESSION:
					// 	// TODO Session-based Authentication
					// 	config.headers.Authorization = `Bearer `;
					// 	break;

					// case AuthBackend.KERBEROS:
					// 	// TODO Kerberos Authentication
					// 	config.headers.Authorization = `Negotiate `;
					// 	break;

					case AuthBackend.TOKEN:
						// Custom Token-based Authentication
						config.headers.Authorization = `Bearer ${this.authConfig.token}`;
						break;

					default:
						throw new Error("Unsupported authentication method");
				}

				return config;
			},
			(error) => {
				return Promise.reject(error);
			},
		);
	}

	private axiosClient: AxiosInstance = axios.create({
		baseURL: "",
	});

	/**
	 * Fetches all DAGs.
	 * @returns {Promise<any>} A promise that resolves to the list of DAGs.
	 */
	getDags = () => this.axiosClient.get("/dags").then((res) => res.data);

	/**
	 * Fetches a specific DAG by its ID.
	 * @param {string} dagId - The ID of the DAG to fetch.
	 * @returns {Promise<any>} A promise that resolves to the DAG details.
	 */
	getDag = (dagId: string) =>
		this.axiosClient.get(`/dags/${dagId}`).then((res) => res.data);

	/**
	 * Creates a new DAG run.
	 * @param {string} dagId - The ID of the DAG to run.
	 * @param {any} data - The data for the DAG run.
	 * @returns {Promise<any>} A promise that resolves to the created DAG run.
	 */
	createDagRun = (dagId: string, data: any) =>
		axios.post(`/dags/${dagId}/dagRuns`, data).then((res) => res.data);

	/**
	 * Fetches all DAG runs for a specific DAG.
	 * @param {string} dagId - The ID of the DAG.
	 * @returns {Promise<any>} A promise that resolves to the list of DAG runs.
	 */
	getDagRuns = (dagId: string) =>
		this.axiosClient.get(`/dags/${dagId}/dagRuns`).then((res) => res.data);

	/**
	 * Fetches a specific DAG run by its ID.
	 * @param {string} dagId - The ID of the DAG.
	 * @param {string} dagRunId - The ID of the DAG run to fetch.
	 * @returns {Promise<any>} A promise that resolves to the DAG run details.
	 */
	getDagRun = (dagId: string, dagRunId: string) =>
		axios.get(`/dags/${dagId}/dagRuns/${dagRunId}`).then((res) => res.data);

	/**
	 * Fetches all tasks for a specific DAG.
	 * @param {string} dagId - The ID of the DAG.
	 * @returns {Promise<any>} A promise that resolves to the list of tasks.
	 */
	getTasks = (dagId: string) =>
		this.axiosClient.get(`/dags/${dagId}/tasks`).then((res) => res.data);

	/**
	 * Fetches a specific task by its ID.
	 * @param {string} dagId - The ID of the DAG.
	 * @param {string} taskId - The ID of the task to fetch.
	 * @returns {Promise<any>} A promise that resolves to the task details.
	 */
	getTask = (dagId: string, taskId: string) =>
		axios.get(`/dags/${dagId}/tasks/${taskId}`).then((res) => res.data);

	/**
	 * Fetches all task instances for a specific DAG run.
	 * @param {string} dagId - The ID of the DAG.
	 * @param {string} dagRunId - The ID of the DAG run.
	 * @returns {Promise<any>} A promise that resolves to the list of task instances.
	 */
	getTaskInstances = (dagId: string, dagRunId: string) =>
		axios
			.get(`/dags/${dagId}/dagRuns/${dagRunId}/taskInstances`)
			.then((res) => res.data);

	/**
	 * Fetches all connections.
	 * @returns {Promise<any>} A promise that resolves to the list of connections.
	 */
	getConnections = () =>
		this.axiosClient.get("/connections").then((res) => res.data);

	/**
	 * Fetches a specific connection by its ID.
	 * @param {string} connectionId - The ID of the connection to fetch.
	 * @returns {Promise<any>} A promise that resolves to the connection details.
	 */
	getConnection = (connectionId: string) =>
		axios.get(`/connections/${connectionId}`).then((res) => res.data);

	/**
	 * Creates a new connection.
	 * @param {any} data - The data for the new connection.
	 * @returns {Promise<any>} A promise that resolves to the created connection.
	 */
	createConnection = (data: any) =>
		axios.post("/connections", data).then((res) => res.data);

	/**
	 * Updates an existing connection.
	 * @param {string} connectionId - The ID of the connection to update.
	 * @param {any} data - The updated data for the connection.
	 * @returns {Promise<any>} A promise that resolves to the updated connection.
	 */
	updateConnection = (connectionId: string, data: any) =>
		axios.patch(`/connections/${connectionId}`, data).then((res) => res.data);

	/**
	 * Deletes a specific connection by its ID.
	 * @param {string} connectionId - The ID of the connection to delete.
	 * @returns {Promise<any>} A promise that resolves to the deletion result.
	 */
	deleteConnection = (connectionId: string) =>
		axios.delete(`/connections/${connectionId}`).then((res) => res.data);

	/**
	 * Fetches all variables.
	 * @returns {Promise<any>} A promise that resolves to the list of variables.
	 */
	getVariables = () =>
		this.axiosClient.get("/variables").then((res) => res.data);

	/**
	 * Fetches a specific variable by its key.
	 * @param {string} variableKey - The key of the variable to fetch.
	 * @returns {Promise<any>} A promise that resolves to the variable details.
	 */
	getVariable = (variableKey: string) =>
		axios.get(`/variables/${variableKey}`).then((res) => res.data);

	/**
	 * Creates a new variable.
	 * @param {any} data - The data for the new variable.
	 * @returns {Promise<any>} A promise that resolves to the created variable.
	 */
	createVariable = (data: any) =>
		axios.post("/variables", data).then((res) => res.data);

	/**
	 * Updates an existing variable.
	 * @param {string} variableKey - The key of the variable to update.
	 * @param {any} data - The updated data for the variable.
	 * @returns {Promise<any>} A promise that resolves to the updated variable.
	 */
	updateVariable = (variableKey: string, data: any) =>
		axios.patch(`/variables/${variableKey}`, data).then((res) => res.data);

	/**
	 * Deletes a specific variable by its key.
	 * @param {string} variableKey - The key of the variable to delete.
	 * @returns {Promise<any>} A promise that resolves to the deletion result.
	 */
	deleteVariable = (variableKey: string) =>
		axios.delete(`/variables/${variableKey}`).then((res) => res.data);

	/**
	 * Fetches all pools.
	 * @returns {Promise<any>} A promise that resolves to the list of pools.
	 */
	getPools = () => this.axiosClient.get("/pools").then((res) => res.data);

	/**
	 * Fetches a specific pool by its name.
	 * @param {string} poolName - The name of the pool to fetch.
	 * @returns {Promise<any>} A promise that resolves to the pool details.
	 */
	getPool = (poolName: string) =>
		this.axiosClient.get(`/pools/${poolName}`).then((res) => res.data);

	/**
	 * Creates a new pool.
	 * @param {any} data - The data for the new pool.
	 * @returns {Promise<any>} A promise that resolves to the created pool.
	 */
	createPool = (data: any) =>
		axios.post("/pools", data).then((res) => res.data);

	/**
	 * Updates an existing pool.
	 * @param {string} poolName - The name of the pool to update.
	 * @param {any} data - The updated data for the pool.
	 * @returns {Promise<any>} A promise that resolves to the updated pool.
	 */
	updatePool = (poolName: string, data: any) =>
		axios.patch(`/pools/${poolName}`, data).then((res) => res.data);

	/**
	 * Deletes a specific pool by its name.
	 * @param {string} poolName - The name of the pool to delete.
	 * @returns {Promise<any>} A promise that resolves to the deletion result.
	 */
	deletePool = (poolName: string) =>
		axios.delete(`/pools/${poolName}`).then((res) => res.data);

	/**
	 * Fetches logs for a specific task instance.
	 * @param {string} dagId - The ID of the DAG.
	 * @param {string} dagRunId - The ID of the DAG run.
	 * @param {string} taskId - The ID of the task.
	 * @param {number} tryNumber - The try number of the task instance.
	 * @returns {Promise<any>} A promise that resolves to the logs.
	 */
	getLogs = (
		dagId: string,
		dagRunId: string,
		taskId: string,
		tryNumber: number,
	) =>
		axios
			.get(
				`/dags/${dagId}/dagRuns/${dagRunId}/taskInstances/${taskId}/logs/${tryNumber}`,
			)
			.then((res) => res.data);

	/**
	 * Fetches the health status of the Airflow instance.
	 * @returns {Promise<any>} A promise that resolves to the health status.
	 */
	getHealth = () => this.axiosClient.get("/health").then((res) => res.data);
}