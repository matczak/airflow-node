import type { AuthBackend } from "./AuthBackend";

export interface AuthConfig {
    method: AuthBackend;
    username?: string;
    password?: string;
    token?: string;
    keytab?: string;
};